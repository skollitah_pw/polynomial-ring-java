package pl.edu.pw.okno.aibd;

import cc.redberry.rings.Rings;
import cc.redberry.rings.poly.FiniteField;
import cc.redberry.rings.poly.PolynomialMethods;
import cc.redberry.rings.poly.univar.UnivariatePolynomial;
import cc.redberry.rings.poly.univar.UnivariatePolynomialZp64;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static pl.edu.pw.okno.aibd.PolynomialConverter.intToPoly;

public class PolynomialTest {

  private static final FiniteField<UnivariatePolynomialZp64> GF = Rings.GF(2, 8);
  private static final UnivariatePolynomial<UnivariatePolynomialZp64> M1 =
      UnivariatePolynomial.parse(intToPoly(0x11B), GF, "x");

  @Test
  public void shouldSolveAibdCasesA() {
    var result = new Solver().solve(0x57, 0x02);
    Assertions.assertEquals("x+x^2+x^3+x^5+x^7", result);
    Assertions.assertEquals(intToPoly(0xAE), result);
  }

  @Test
  public void shouldSolveAibdCasesB() {
    var result = new Solver().solve(0x57, 0x04);
    Assertions.assertEquals("1+x+x^2+x^6", result);
    Assertions.assertEquals(intToPoly(0x47), result);
  }

  @Test
  public void shouldSolveAibdCasesC() {
    var result = new Solver().solve(0x57, 0x10);
    Assertions.assertEquals("1+x+x^2", result);
    Assertions.assertEquals(intToPoly(0x07), result);
  }

  @Test
  public void shouldSolveAibdCasesX() {
    var result = new Solver().solve(0x57, 0x08);
    Assertions.assertEquals("x+x^2+x^3+x^7", result);
    Assertions.assertEquals(intToPoly(0x8E), result);
  }

  @Test
  public void shouldAddAndMultiplyPolynomials() {
    var a = UnivariatePolynomial.parse("x^6 + x^4 + x^2 + x + 1", GF, "x");
    var b = UnivariatePolynomial.parse("x^7 + x + 1", GF, "x");
    var p3 = a.copy().add(b);
    var p4 = a.copy().multiply(b);
    var p5 = PolynomialMethods.divideAndRemainder(a.copy().multiply(b), M1);
    System.out.println(p3.toString("x"));
    System.out.println(p4.toString("x"));
    System.out.println(p5[1].toString("x"));
    Assertions.assertEquals("1+x^6+x^7", p5[1].toString("x"));
  }

  @Test
  public void shouldAddAndMultiplyPolynomialsFromHex() {
    var p1 = UnivariatePolynomial.parse(intToPoly(0x57), GF, "x");
    var p2 = UnivariatePolynomial.parse(intToPoly(0x83), GF, "x");
    var m1 = UnivariatePolynomial.parse(intToPoly(0x11B), GF, "x");
    var p3 = p1.copy().add(p2);
    var p4 = p1.copy().multiply(p2);
    var p5 = PolynomialMethods.divideAndRemainder(p1.copy().multiply(p2), m1);
    System.out.println(p3.toString("x"));
    System.out.println(p4.toString("x"));
    System.out.println(p5[1].toString("x"));
    Assertions.assertEquals("1+x^6+x^7", p5[1].toString("x"));
  }

  @Test
  public void testIntToPoly() {
    Assertions.assertEquals(intToPoly(0x57), "1+x+x^2+x^4+x^6");
    Assertions.assertEquals(intToPoly(0x83), "1+x+x^7");
    Assertions.assertEquals(intToPoly(0x11B), "1+x+x^3+x^4+x^8");
    Assertions.assertEquals(intToPoly(0xC1), "1+x^6+x^7");
  }
}
