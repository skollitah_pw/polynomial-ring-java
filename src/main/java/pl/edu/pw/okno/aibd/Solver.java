package pl.edu.pw.okno.aibd;

import cc.redberry.rings.Rings;
import cc.redberry.rings.poly.FiniteField;
import cc.redberry.rings.poly.PolynomialMethods;
import cc.redberry.rings.poly.univar.UnivariatePolynomial;
import cc.redberry.rings.poly.univar.UnivariatePolynomialZp64;

import static pl.edu.pw.okno.aibd.PolynomialConverter.intToPoly;

public class Solver {
  private static final FiniteField<UnivariatePolynomialZp64> GF = Rings.GF(2, 8);
  private static final UnivariatePolynomial<UnivariatePolynomialZp64> M1 =
      UnivariatePolynomial.parse(intToPoly(0x11B), GF, "x");

  public String solve(int a, int b) {
    var aPoly = UnivariatePolynomial.parse(intToPoly(a), GF, "x");
    var bPoly = UnivariatePolynomial.parse(intToPoly(b), GF, "x");
    var result = PolynomialMethods.divideAndRemainder(aPoly.copy().multiply(bPoly), M1);
    return result[1].toString("x");
  }
}
