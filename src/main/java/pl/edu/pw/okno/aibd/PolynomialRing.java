package pl.edu.pw.okno.aibd;

public class PolynomialRing {

  public static void main(String[] args) {
    if (args.length != 2) {
      throw new IllegalArgumentException("Invalid number of arguments");
    }

    int a = Integer.decode(args[0]);
    int b = Integer.decode(args[1]);

    System.out.println(new Solver().solve(a, b));
  }
}
