package pl.edu.pw.okno.aibd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

public class PolynomialConverter {

  public static String intToPoly(int number) {
    var bitArray =
        Arrays.stream(Integer.toBinaryString(number).split("")).collect(Collectors.toList());
    Collections.reverse(bitArray);
    var poly = new ArrayList<String>();

    for (int i = 0; i < bitArray.size(); i++) {
      if (bitArray.get(i).equals("1")) {
        if (i == 0) {
          poly.add("" + 1);
        } else if (i == 1) {
          poly.add("x");
        } else {
          poly.add("x^" + i);
        }
      }
    }

    return String.join("+", poly);
  }
}
