Build
```sh
./gradlew fatJar
```

Run example
```sh
cd build/libs/
java -jar polynomial-ring-java-1.0-SNAPSHOT-all.jar 0x57 0x83
```